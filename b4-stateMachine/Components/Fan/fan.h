#ifndef FAN_FAN_H_
#define FAN_FAN_H_

#include <core/engine.h>
#include <core/machine.h>
#include <core/task.h>

MACHINE(ex, Fan)

	M_TASK(timeout, {SM_POST(Event::Timeout);})
	M_TASK(plot)

public:
	void init() override;
	void setSpeed(uint16_t value)
	{
		setSpeed_ = value;
		SM_POST(Event::SpeedChange);
	}

private:
	enum class Event{SpeedChange, Timeout};

private:
	STATE_DEF(StartUp)
	STATE_DEF(Ramping)
	STATE_DEF(Ready)

private:
	uint16_t speed_;
	uint16_t setSpeed_;

MACHINE_END

#endif /* FAN_FAN_H_ */
