#include <Fan/fan.h>
#include <console/controller.h>
#include <console/log.h>

void ex::Fan::init()
{
	plotTask_.start(1);
	SM_START(StartUp);
}

M_TASK_HANDLER(ex::Fan, plot)
{
	MC_PLOT(0, speed_);
}
