#include <Fan/fan.h>
#include <console/controller.h>
#include <console/log.h>

STATE_BODY(ex::Fan::StartUp)
{
	ENTER_()
	{
//		LOG_PRINTF("FAN: StartUp");
		timeoutTask_.start(100,1);
	}
	TRANSITION_(Event::Timeout, Ramping){}
}

STATE_BODY(ex::Fan::Ramping)
{
	ENTER_()
	{
		timeoutTask_.start(10);
	}
	TRANSITION_(Event::Timeout)
	{
		if(speed_ < setSpeed_)speed_++;
		else if(speed_ > setSpeed_)speed_--;
		else
		{
			SM_SWITCH(Ready);
		}
	}
	EXIT_(){timeoutTask_.stop();}
}

STATE_BODY(ex::Fan::Ready)
{
	ENTER_()
	{
		LOG_PRINTF("FAN: Ready");
	}
	TRANSITION_(Event::SpeedChange, Ramping){}
}
