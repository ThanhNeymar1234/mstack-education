#include <lcd/lcd.h>
#include "gpio.h"
#include <console/log.h>

void lcd::Driver::init()
{
	HAL_GPIO_WritePin(RS_PORT, RS_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(E_PORT, E_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(D4_PORT, D4_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(D5_PORT, D5_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(D6_PORT, D6_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(D7_PORT, D7_PIN, GPIO_PIN_RESET);

	SM_START(StartUp);
}

void lcd::Driver::write2Nib(uint8_t data)
{
	uint8_t hNib, lNib;
	hNib = data >> 4;
	lNib = data & 0x0F;

	HAL_GPIO_WritePin(E_PORT, E_PIN, GPIO_PIN_SET);

	HAL_GPIO_WritePin(D7_PORT, D7_PIN, (GPIO_PinState)((hNib>>3)&0x01));
	HAL_GPIO_WritePin(D6_PORT, D6_PIN, (GPIO_PinState)((hNib>>2)&0x01));
	HAL_GPIO_WritePin(D5_PORT, D5_PIN, (GPIO_PinState)((hNib>>1)&0x01));
	HAL_GPIO_WritePin(D4_PORT, D4_PIN, (GPIO_PinState)((hNib)&0x01));

	HAL_GPIO_WritePin(E_PORT, E_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(E_PORT, E_PIN, GPIO_PIN_SET);

	HAL_GPIO_WritePin(D7_PORT, D7_PIN, (GPIO_PinState)((lNib>>3)&0x01));
	HAL_GPIO_WritePin(D6_PORT, D6_PIN, (GPIO_PinState)((lNib>>2)&0x01));
	HAL_GPIO_WritePin(D5_PORT, D5_PIN, (GPIO_PinState)((lNib>>1)&0x01));
	HAL_GPIO_WritePin(D4_PORT, D4_PIN, (GPIO_PinState)((lNib)&0x01));

	HAL_GPIO_WritePin(E_PORT, E_PIN, GPIO_PIN_RESET);
}

void lcd::Driver::write8Bit(uint8_t data)
{
	// TODO: use in 8bit mode
}

void lcd::Driver::send(uint8_t type, uint8_t data)
{
	Data tmp;
	tmp.data = data; tmp.type = type;
	commandStrand.post<Data>(&SendEvent, tmp);
}

M_EVENT_HANDLER(lcd::Driver, Send, Data)
{
	data_ = event;
	SM_POST(Event::Send);
}
