#include <lcd/controller.h>
#include <lcd/lcd.h>
#include <string.h>

void lcd::Controller::init()
{
	Driver::instance().init();
	Driver::instance().commandStrand.delay(200);
	init4Bit();
	clear();
}

void lcd::Controller::init4Bit()
{
	Driver::instance().send(SEND_CMD, 0x20);	// fake Command
	Driver::instance().send(SEND_CMD, M_4_BIT);
	Driver::instance().send(SEND_CMD, DISPLAY_CONTROL);
	Driver::instance().send(SEND_CMD, ENTRY_MODE);
}

void lcd::Controller::clear()
{
	Driver::instance().send(SEND_CMD, CLEAR_LCD);
}

void lcd::Controller::home()
{
	Driver::instance().send(SEND_CMD, HOME_CURSOR);
}

void lcd::Controller::gotoxy(uint8_t col, uint8_t row)
{
	uint16_t address;

    if(row!=0){address=0x40;}
    else{address=0;}

    address += col;

//	pos=64*(x-1)+(y-1)+0x80; // tính mã lệnh
	Driver::instance().send(SEND_CMD, 0x80|address);
}

void lcd::Controller::putchar(char data)
{
	Driver::instance().send(SEND_DATA, data);
}

void lcd::Controller::printf(char* data)
{
	uint8_t i, len;
	len = strlen(data);
	for (i=0; i<len; i++)
	{
		if(data[i] > 0) putchar(data[i]);
		else putchar(' ');
	}
}
