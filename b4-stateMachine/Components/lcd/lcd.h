#ifndef LCD1602_LCD_H_
#define LCD1602_LCD_H_

#include <core/engine.h>
#include <core/machine.h>
#include <core/strand.h>
#include <core/event.h>
#include <core/task.h>
#include <lcd/lcd-define.h>

MACHINE(lcd, Driver)

	M_TASK(timeout,{SM_POST(Event::Timeout);})
	M_STRAND(command, 512)

public:
	void init();

	void write2Nib(uint8_t data);
	void write8Bit(uint8_t data);
	void send(uint8_t type, uint8_t data);

public:
	struct Data
	{
		uint8_t type;
		uint8_t data;
	};

	M_EVENT(Send, Data)

public:
	enum class Event{Timeout, Send};
	enum class Command{};
	enum class Type{Command = 0, Data};
private:
	STATE_DEF(StartUp)
	STATE_DEF(Ready)
	STATE_DEF(Busy)

private:
	Data data_;

MACHINE_END

#endif /* LCD1602_LCD_H_ */
