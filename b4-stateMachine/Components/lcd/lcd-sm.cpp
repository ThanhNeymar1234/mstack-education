#include <lcd/lcd.h>
#include <console/log.h>
#include "gpio.h"

STATE_BODY(lcd::Driver::StartUp)
{
	ENTER_()
	{
//		LOG_PRINTF("LCD: StartUp");
		timeoutTask_.start(100,1);
	}
	TRANSITION_(Event::Timeout,Ready){}
}

STATE_BODY(lcd::Driver::Ready)
{
	ENTER_()
	{
//		LOG_PRINTF("LCD: Ready");
		commandStrand.done();
	}
	TRANSITION_(Event::Send, Busy)
	{
		if(data_.type == SEND_CMD)
		{
			HAL_GPIO_WritePin(RS_PORT, RS_PIN, GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(RS_PORT, RS_PIN, GPIO_PIN_SET);
		}
		write2Nib(data_.data);
	}
}

STATE_BODY(lcd::Driver::Busy)
{
	ENTER_()
	{
//		LOG_PRINTF("LCD: Busy");
		timeoutTask_.start(2,1);
	}
	TRANSITION_(Event::Timeout, Ready){}
}

