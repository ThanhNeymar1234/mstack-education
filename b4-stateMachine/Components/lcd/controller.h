#ifndef LCD1602_CONTROLLER_H_
#define LCD1602_CONTROLLER_H_

#include <core/engine.h>
#include <lcd/lcd.h>

COMPONENT(lcd, Controller)

public:
	void init() override;
	void init4Bit();
	void init8Bit(){};

	void home();						// cursor goto home position
	void gotoxy(uint8_t x, uint8_t y);	// move cursor to x,y
	void clear();
	void putchar(char data);
	void printf(char* data);

private:

COMPONENT_END

#endif /* LCD1602_CONTROLLER_H_ */
