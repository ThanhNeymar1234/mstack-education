#ifndef DHT22_DHT_H_
#define DHT22_DHT_H_

#include <core/engine.h>
#include <core/machine.h>
#include <console/controller.h>
#include <console/log.h>
#include <core/signal.h>
#include "tim.h"

MACHINE(bin, DHT22)

	U_ACTION(999, read)
	M_TASK(readSensor)

public:
	void init() override;
	uint32_t getCount(){return ++this->count_;}
	uint32_t getSetCount(){return this->setCount_;}
	inline void waitDoneExecute(){SM_POST(Event::WaitDone);}
	inline void resetCount(){count_ = 0;}
	inline void startTimer(uint32_t value)
	{
		setCount_ = value;
		TIM1->CNT = 0;
		LL_TIM_EnableCounter(TIM1);
	}
	struct Data
	{
		uint16_t temp;
		uint16_t humid;
	};

	M_SIGNAL(sensorData, Data)

private:
	void parse_();
	enum class Event{Start, WaitDone};
private:
	STATE_DEF(Idle)
	STATE_DEF(Start)
	STATE_DEF(CheckStartDone)
	STATE_DEF(CheckResponse1)
	STATE_DEF(CheckResponse2)
	STATE_DEF(WaitingData)
	STATE_DEF(ReceiveData)
	STATE_DEF(WaitEnd)
private:
	Data dataSensor_;
	uint32_t count_, setCount_ = 1;
	uint16_t retries_, row_, column_;
	uint8_t data_[5];
	/*
	 * data_[0]: Humid Integral	0000 0010
	 * data_[1]: Humid Decimal  1000 1100
	 * data_[2]: Temp Integral  0000 0001
	 * data_[3]: Temp Decimal   0101 1111
	 * data_[4]: CheckSum*/   //1110 1110

MACHINE_END

#endif /* DHT22_DHT_H_ */
