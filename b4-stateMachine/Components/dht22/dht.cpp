#include <dht22/dht.h>
#include <console/log.h>
#include <console/controller.h>

void bin::DHT22::init()
{
	MX_TIM1_Init();
	LL_TIM_EnableIT_UPDATE(TIM1);
	SM_START(Idle);
	readSensorTask_.start(2000);
}

void bin::DHT22::parse_()
{
	uint16_t fakeSum = data_[0] + data_[1] + data_[2] + data_[3];
	if(fakeSum > 255)fakeSum -= 256;
	if(fakeSum == data_[4])
	{
		dataSensor_.humid = (((data_[0]<<8)&0xFF00)|data_[1]);
		dataSensor_.temp = (((data_[2]<<8)&0xFF00)|data_[3]);
//		LOG_PRINTF("Humid: %d",dataSensor_.humid);
//		LOG_PRINTF("Temp : %d",dataSensor_.temp);
//		LOG_PRINTF("FS : %d, CS: %d",fakeSum, data_[4]);
//		MC_PLOT(0, dataSensor_.humid);
//		MC_PLOT(1, dataSensor_.temp);
		sensorDataSignal.emit(dataSensor_);

		for(row_ = 0; row_ < 5; row_++){data_[row_] = 0;}
		row_ = column_ = 0;
	}
	else
	{
//		LOG_PRINTF("FS : %d, CS: %d",fakeSum, data_[4]);
//		LOG_PRINTF("Wrong Check Sum");
	}
}

M_TASK_HANDLER(bin::DHT22, readSensor)
{
	SM_POST(Event::Start);
}

U_ACTION_HANDLER(bin::DHT22, read)
{
	LOG_PRINTF("Read DHT22");
	SM_POST(Event::Start);
}

extern "C" void TIM1_UP_TIM10_IRQHandler(void)	// 10us
{
	static bin::DHT22& dht22 = bin::DHT22::instance();
	if(LL_TIM_IsActiveFlag_UPDATE(TIM1))
	{
		LL_TIM_ClearFlag_UPDATE(TIM1);
		if(dht22.getCount() == dht22.getSetCount())
		{
			dht22.waitDoneExecute();
			dht22.resetCount();
			LL_TIM_DisableCounter(TIM1);
		}
	}
}
