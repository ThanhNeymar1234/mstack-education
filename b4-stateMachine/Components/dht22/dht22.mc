Project{name:'Read DHT22'}

Section{name:'Pannel'}
Button{name: 'A', command: 100}
Button{name: 'B', command: 101}

Plot{name:'Humid', color:'magenta',channel:0,scale: 1, offset: 800}
Plot{name:'Temp', color:'cyan',channel:1,scale: 1, offset: 300}