#include <console/controller.h>
#include <dht22/dht.h>

int main()
{
	COMPONENT_REG(console, Controller);
	COMPONENT_REG(dht, Driver);
	core::Engine::instance().run();
	return 0;
}
