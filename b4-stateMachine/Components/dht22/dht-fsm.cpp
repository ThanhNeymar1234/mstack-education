#include <dht22/dht.h>
#include <console/log.h>
#include "gpio.h"

STATE_BODY(bin::DHT22::Idle)
{
	ENTER_()
	{
//		LOG_PRINTF("Idle");
		LL_GPIO_SetPinMode(DHT22_GPIO_Port, DHT22_Pin, LL_GPIO_MODE_OUTPUT);
		LL_GPIO_SetOutputPin(DHT22_GPIO_Port, DHT22_Pin);
	}
	TRANSITION_(Event::Start, Start)
	{
		LL_GPIO_ResetOutputPin(DHT22_GPIO_Port, DHT22_Pin);
	}
}

STATE_BODY(bin::DHT22::Start)
{
	ENTER_()
	{
//		LOG_PRINTF("Start");
		startTimer(500);	// pull low 5ms
	}
	TRANSITION_(Event::WaitDone,CheckStartDone)
	{
		LL_GPIO_SetOutputPin(DHT22_GPIO_Port, DHT22_Pin);
	}
}

STATE_BODY(bin::DHT22::CheckStartDone)
{
	ENTER_()
	{
//		LOG_PRINTF("CheckStartDone");
		LL_GPIO_SetPinMode(DHT22_GPIO_Port, DHT22_Pin, LL_GPIO_MODE_INPUT);
		startTimer(1);	// wait 10us to check PinDHT
	}
	TRANSITION_(Event::WaitDone)
	{
		if(LL_GPIO_IsInputPinSet(DHT22_GPIO_Port, DHT22_Pin))	// = 1
		{
			if(++retries_ >= 10)	//TODO: fail
			{
				LOG_PRINTF("Fail 0");
				SM_SWITCH(Idle);
			}
			else{startTimer(1);}
		}
		else
		{
			SM_SWITCH(CheckResponse1);
		}
	}
	EXIT_(){retries_ = 0;}
}

STATE_BODY(bin::DHT22::CheckResponse1)
{
	ENTER_()
	{
//		LOG_PRINTF("CheckResponse1");
		startTimer(1);
	}
	TRANSITION_(Event::WaitDone)
	{
		if(LL_GPIO_IsInputPinSet(DHT22_GPIO_Port, DHT22_Pin))	// = 1
		{
			SM_SWITCH(CheckResponse2);
		}
		else
		{
			if(++retries_ >= 10)	//TODO: fail
			{
				LOG_PRINTF("Fail 1");
				SM_SWITCH(Idle);
			}
			else{startTimer(1);}
		}
	}
	EXIT_(){retries_ = 0;}
}

STATE_BODY(bin::DHT22::CheckResponse2)
{
	ENTER_()
	{
//		LOG_PRINTF("CheckResponse2");
		startTimer(1);
	}
	TRANSITION_(Event::WaitDone)
	{
		if(LL_GPIO_IsInputPinSet(DHT22_GPIO_Port, DHT22_Pin))	// = 1
		{
			if(++retries_ >= 10)	//TODO: fail
			{
				LOG_PRINTF("Fail 2");
				SM_SWITCH(Idle);
			}
			else{startTimer(1);}
		}
		else
		{
			SM_SWITCH(WaitingData);
		}
	}
	EXIT_(){retries_ = 0;}
}

STATE_BODY(bin::DHT22::WaitingData)
{
	ENTER_()
	{
//		LOG_PRINTF("WaitingData");
		startTimer(1);	//FIXME: 1
	}
	TRANSITION_(Event::WaitDone)
	{
		if(LL_GPIO_IsInputPinSet(DHT22_GPIO_Port, DHT22_Pin))	// = 1
		{
			SM_SWITCH(ReceiveData);
		}
		else
		{
			if(++retries_ >= 10)	//TODO: fail
			{
				LOG_PRINTF("Fail 3");
				SM_SWITCH(Idle);
			}
			else{startTimer(1);}	// FIXME: 1
		}
	}
	EXIT_(){retries_ = 0;}
}

STATE_BODY(bin::DHT22::ReceiveData)
{
	ENTER_()
	{
//		LOG_PRINTF("ReceiveData");
		startTimer(3);	// wait 30us to check Pin
	}
	TRANSITION_(Event::WaitDone)
	{
		if(LL_GPIO_IsInputPinSet(DHT22_GPIO_Port, DHT22_Pin))	// = 1
		{
			// data 1
			data_[row_] |= 1<<(7 - column_);
			column_++;
			SM_SWITCH(CheckResponse2);
		}
		else
		{
			// data 0
			data_[row_] |= 0<<(7 - column_);
			column_++;
			SM_SWITCH(WaitingData);
		}
		if(column_ == 8){row_++;column_ = 0;}
		if(row_ == 5)
		{
			SM_SWITCH(WaitEnd);
		}
	}
}

STATE_BODY(bin::DHT22::WaitEnd)
{
	ENTER_()
	{
//		LOG_PRINTF("WaitEnd");
		parse_();
		startTimer(15);	// wait 150us to re pull high bus
	}
	TRANSITION_(Event::WaitDone, Idle){}
}

