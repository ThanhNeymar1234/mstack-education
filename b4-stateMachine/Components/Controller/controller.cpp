#include <Controller/controller.h>
#include <dht22/dht.h>
#include <Fan/fan.h>

void ex::Controller::init()
{
	COMPONENT_REG(console, Controller);
	COMPONENT_REG(bin, DHT22);
	COMPONENT_REG(ex, Fan);
	SM_START(StartUp);
}

U_ACTION_HANDLER(ex::Controller, speedup)
{
	LOG_PRINTF("Speed Up");
	SM_POST(Event::SpeedUp);
}

U_ACTION_HANDLER(ex::Controller, speeddown)
{
	LOG_PRINTF("Speed Down");
	SM_POST(Event::SpeedDown);
}

U_ACTION_HANDLER(ex::Controller, autoMode)
{
	LOG_PRINTF("Switch to Auto");
	SM_POST(Event::ChangeToAuto);
}

U_ACTION_HANDLER(ex::Controller, manualMode)
{
	LOG_PRINTF("Speed To Manual");
	SM_POST(Event::ChangeToManual);
}

M_EVENT_HANDLER(ex::Controller, dataDHT, bin::DHT22::Data)
{
	temp_ = event.temp;
	SM_POST(Event::AutoCalculateSpeed);
}
