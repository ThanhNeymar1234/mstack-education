#include <Controller/controller.h>
#include <Fan/fan.h>
#include <console/log.h>

STATE_BODY(ex::Controller::StartUp)
{
	ENTER_()
	{
		LOG_PRINTF("Controller: StartUp");
		timeoutTask_.start(1000,1);
	}
	TRANSITION_(Event::Timeout)
	{
		if(mode_ == Mode::Auto)SM_SWITCH(Auto);
		else if(mode_ == Mode::Manual)SM_SWITCH(Manual);
	}
}

STATE_BODY(ex::Controller::Auto)
{
	ENTER_()
	{
		LOG_PRINTF("Controller: Auto");
		bin::DHT22::instance().sensorDataSignal.connect(&dataDHTEvent);
	}

	TRANSITION_(Event::AutoCalculateSpeed)
	{
		speedSet_ = temp_;
		if(speedSet_ >= 5000)speedSet_ = 5000;
		if(speedSet_ <= 200)speedSet_ = 200;
		Fan::instance().setSpeed(speedSet_);
	}

	TRANSITION_(Event::ChangeToManual, Manual)
	{
		bin::DHT22::instance().sensorDataSignal.disconnect();
	}
}

STATE_BODY(ex::Controller::Manual)
{
	ENTER_()
	{
		LOG_PRINTF("Controller: Manual");
	}

	TRANSITION_(Event::SpeedUp)
	{
		speedSet_+=200;
		if(speedSet_ >= 5000)speedSet_ = 5000;
		Fan::instance().setSpeed(speedSet_);
	}

	TRANSITION_(Event::SpeedDown)
	{
		speedSet_-=200;
		if(speedSet_ <= 200)speedSet_ = 200;
		Fan::instance().setSpeed(speedSet_);
	}
}
