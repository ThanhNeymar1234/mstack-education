#ifndef CONTROLLER_CONTROLLER_H_
#define CONTROLLER_CONTROLLER_H_

#include <core/engine.h>
#include <core/machine.h>
#include <core/task.h>
#include <console/controller.h>
#include <core/event.h>
#include <dht22/dht.h>

MACHINE(ex, Controller)

	M_TASK(timeout, {SM_POST(Event::Timeout);})
	M_EVENT(dataDHT, bin::DHT22::Data)

public:
	void init() override;

private:
	enum class Event{Timeout, ChangeToAuto, ChangeToManual, AutoCalculateSpeed,
					SpeedUp, SpeedDown};
	enum class Mode{Auto = 0, Manual};
	enum class Command{SpeedUp = 100, SpeedDown, Auto, Manual};

	U_ACTION(Command::Auto, autoMode)
	U_ACTION(Command::Manual, manualMode)

	U_ACTION(Command::SpeedUp, speedup)
	U_ACTION(Command::SpeedDown, speeddown)

private:
	Mode mode_ = Mode::Auto;
	uint16_t temp_;
	uint16_t speedSet_ = 500;

private:
	STATE_DEF(StartUp)
	STATE_DEF(Auto)
	STATE_DEF(Manual)

MACHINE_END


#endif /* CONTROLLER_CONTROLLER_H_ */
