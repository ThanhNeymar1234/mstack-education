#ifndef LED_INDICATOR_LED_H_
#define LED_INDICATOR_LED_H_

#include <core/engine.h>
#include <core/task.h>

COMPONENT(ex, Led)

	M_TASK(blinkLed1)
	M_TASK(blinkLed2)
	M_TASK(blinkLed3)

	M_TASK(plot)

public:
	void init() override;

private:
	uint16_t interval1_;
	uint16_t interval2_;
	uint16_t interval3_;
	bool state1_ = false;
	bool state2_ = false;
	bool state3_ = false;
	uint8_t soLan_ = 0;

COMPONENT_END

#endif /* LED_INDICATOR_LED_H_ */
