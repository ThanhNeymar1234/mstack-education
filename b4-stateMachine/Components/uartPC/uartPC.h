#ifndef UARTPC_UARTPC_H_
#define UARTPC_UARTPC_H_

#include <core/engine.h>
#include <core/machine.h>
#include <core/event.h>

#define QUEUE_SIZE	256
#define RX_MAX_SIZE	256

#define HEADER		0xFF
#define FOOTER		0xFE

MACHINE(ex, uartPC)

	M_EVENT(send)
	M_EVENT(rxReceive, uint8_t)

public:
	void init() override;
	void putchar(char c);
	void printf(char *data/*, uint16_t length*/);

	enum class Event{RxReceive};

private:
	void process_();

private:
	STATE_DEF(ReceiveHeader)
	STATE_DEF(ReceiveLength)
	STATE_DEF(ReceiveData)
	STATE_DEF(ReceiveFooter)

private:
	QUEUE_DEF(commands, QUEUE_SIZE);
	bool sending_ = false;

private:
	uint8_t data_;
	uint8_t rxLength_;
	uint8_t rxBuffer_[RX_MAX_SIZE];
	uint8_t rxIndex_;

MACHINE_END

#endif /* UARTPC_UARTPC_H_ */
