#include <uartPC/uartPC.h>
#include <uartPC/hal.h>
#include <string.h>

void ex::uartPC::init()
{
	HAL::init();
	SM_START(ReceiveHeader);
}

void ex::uartPC::putchar(char c)
{
	commands_.push(c);
	if(!sending_)
	{
		sendEvent.post();
	}
}

void ex::uartPC::printf(char *data/*, uint16_t length*/)
{
	uint16_t l;
	l = strlen(data);
	for(int i = 0; i< l; i++)
	{
		commands_.push(data[i]);
	}
	if(!sending_)
	{
		sendEvent.post();
	}
}

void ex::uartPC::process_()
{
	if(!memcmp(rxBuffer_, "Hello", 5))
	{
		printf("OK\r\n");
	}
}

M_EVENT_HANDLER(ex::uartPC, send)
{
	if(commands_.empty())
	{
		sending_ = false;
		return;
	}
	if(HAL::txReady())
	{
		HAL::write(commands_.pop());
	}
	sendEvent.post();
}

M_EVENT_HANDLER(ex::uartPC, rxReceive, uint8_t)
{
	data_ = event;
	SM_POST(Event::RxReceive);
}
