#ifndef CONSOLE_HAL_H
#define CONSOLE_HAL_H
#include <core/base.h>
#include <uartPC/uartPC.h>

HAL_DEF(ex)
    static void init();
    static bool txReady();
    static void write(uint8_t c);
HAL_END

#endif // HAL_H
