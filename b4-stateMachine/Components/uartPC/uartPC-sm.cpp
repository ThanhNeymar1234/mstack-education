#include <uartPC/uartPC.h>

STATE_BODY(ex::uartPC::ReceiveHeader)
{
	ENTER_()
	{
		printf("Header\r\n");
	}
	TRANSITION_(Event::RxReceive)
	{
		if(data_ == HEADER)
		{
			SM_SWITCH(ReceiveLength);
		}
	}
}

STATE_BODY(ex::uartPC::ReceiveLength)
{
	ENTER_()
	{
		printf("Length\r\n");
	}

	TRANSITION_(Event::RxReceive)
	{
		rxLength_ = data_;
		if(rxLength_ > RX_MAX_SIZE){SM_SWITCH(ReceiveHeader);}
		else
		{
			SM_SWITCH(ReceiveData);
			rxIndex_ = 0;
		}
	}
}

STATE_BODY(ex::uartPC::ReceiveData)
{
	ENTER_()
	{
		printf("Data\r\n");
	}

	TRANSITION_(Event::RxReceive)
	{
		rxBuffer_[rxIndex_++] = data_;
		if(rxIndex_ == rxLength_){SM_SWITCH(ReceiveFooter);}
	}
}

STATE_BODY(ex::uartPC::ReceiveFooter)
{
	ENTER_()
	{
		printf("Footer\r\n");
	}
	TRANSITION_(Event::RxReceive)
	{
		if(data_ == FOOTER)
		{
			process_();
		}
		SM_SWITCH(ReceiveHeader);
	}
}

