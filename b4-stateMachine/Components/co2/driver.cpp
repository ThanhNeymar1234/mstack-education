#include <co2/driver.h>
#include <console/log.h>
#include "i2c.h"

#define CO2_INTERVAL	3000

void co2::Driver::init()
{
	MX_I2C3_Init();
	SM_START(Driver::Idle);
	readTask_.start(CO2_INTERVAL);
}

void co2::Driver::startRead_()
{
	//LOG_PRINT("Start read Co2");
	LL_I2C_Enable(I2C_CO2_INSTANCE);
	LL_I2C_EnableIT_EVT(I2C_CO2_INSTANCE);
	LL_I2C_AcknowledgeNextData(I2C_CO2_INSTANCE, LL_I2C_ACK);
	LL_I2C_GenerateStartCondition(I2C_CO2_INSTANCE);
}

void co2::Driver::endRead_()
{
	//LOG_PRINT("End read Co2");
	LL_I2C_DisableIT_EVT(I2C_CO2_INSTANCE);
	LL_I2C_DisableIT_ERR(I2C_CO2_INSTANCE);
}

void co2::Driver::calculate_()
{
	uint16_t crc = 0;
	uint16_t checksum = (data_[10] << 8) | data_[11] ;
	for(int i = 0; i < 10; i++)
	{
		crc += data_[i];
	}

	if((data_[0] == CO2_HEADER1) && (data_[1] == CO2_HEADER2) && (crc == checksum))
	{
		uint16_t value = ((data_[4] << 8) | data_[5]);
		dataReceivedSignal.emit(value);
		LOG_PRINTF("CO2: %d", value);
	}
	else
	{
		LOG_PRINT("CO2: Read CO2 Wrong Frame");
		//co2::Controller::instance().error();
	}
}

void co2::Driver::update()
{
	if(LL_I2C_IsActiveFlag_SB(I2C_CO2_INSTANCE)) //start bit
	{
		LL_I2C_TransmitData8(I2C_CO2_INSTANCE, SLAVE_OWN_ADDRESS<<1 | I2C_REQUEST_READ);
	}
	else if(LL_I2C_IsActiveFlag_ADDR(I2C_CO2_INSTANCE))
	{
	    SM_PÓ
		SM_EXECUTE(EventType::FlagADDR);
	}
	else if(LL_I2C_IsActiveFlag_BTF(I2C_CO2_INSTANCE))
	{
		SM_EXECUTE(EventType::FlagBTF);
	}
	else if(LL_I2C_IsActiveFlag_RXNE(I2C_CO2_INSTANCE))
	{
		SM_EXECUTE(EventType::FlagRXNE);
	}
}

extern "C" void I2C3_EV_IRQHandler(void)
{
	co2::Driver::instance().update();
}
