#include <co2/driver.h>
#include "i2c.h"

M_TASK_HANDLER(co2::Driver, read)
{
	SM_POST(EventType::Start);
}

STATE_BODY(co2::Driver::Idle)
{
	TRANSITION_(EventType::Start, ReceiveN)
	{
		startRead_();
		index_ = 0;
	}
}

STATE_BODY(co2::Driver::ReceiveN)
{
	TRANSITION_(EventType::FlagADDR)
	{
		LL_I2C_EnableIT_BUF(I2C_CO2_INSTANCE);
		LL_I2C_ClearFlag_ADDR(I2C_CO2_INSTANCE);
	}

	TRANSITION_(EventType::FlagBTF)
	{
		data_[index_++] = LL_I2C_ReceiveData8(I2C_CO2_INSTANCE);
		if (index_ == CO2_BUFFER_SIZE-3) SM_SWITCH(Receive3);
	}

	TRANSITION_(EventType::FlagRXNE)
	{
		data_[index_++] = LL_I2C_ReceiveData8(I2C_CO2_INSTANCE);
		if (index_ == CO2_BUFFER_SIZE-3) SM_SWITCH(Receive3);
	}
}

STATE_BODY(co2::Driver::Receive3)
{

	TRANSITION_(EventType::FlagADDR)
	{
		LL_I2C_EnableIT_BUF(I2C_CO2_INSTANCE);
		LL_I2C_ClearFlag_ADDR(I2C_CO2_INSTANCE);
	}

	TRANSITION_(EventType::FlagBTF, Receive2)
	{
		LL_I2C_AcknowledgeNextData(I2C_CO2_INSTANCE, LL_I2C_NACK);
		data_[index_++] = LL_I2C_ReceiveData8(I2C_CO2_INSTANCE);
		LL_I2C_DisableIT_BUF(I2C_CO2_INSTANCE);
	}

	TRANSITION_(EventType::FlagRXNE)
	{
		LL_I2C_DisableIT_BUF(I2C_CO2_INSTANCE);
	}
}

STATE_BODY(co2::Driver::Receive2)
{
	TRANSITION_(EventType::FlagADDR)
	{
		LL_I2C_AcknowledgeNextData(I2C_CO2_INSTANCE, LL_I2C_NACK);
		LL_I2C_EnableBitPOS(I2C_CO2_INSTANCE);
		LL_I2C_ClearFlag_ADDR(I2C_CO2_INSTANCE);
	}

	TRANSITION_(EventType::FlagBTF, Idle)
	{
		LL_I2C_GenerateStopCondition(I2C_CO2_INSTANCE);
		data_[index_++] = LL_I2C_ReceiveData8(I2C_CO2_INSTANCE);
		data_[index_++] = LL_I2C_ReceiveData8(I2C_CO2_INSTANCE);
		endRead_();
		this->calculate_();
	}

	TRANSITION_(EventType::FlagRXNE)
	{
		LL_I2C_DisableIT_BUF(I2C_CO2_INSTANCE);
	}
}
