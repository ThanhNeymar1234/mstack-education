#include <core/machine.h>
#include <core/signal.h>

#define CO2_BUFFER_SIZE			12
#define I2C_CO2_INSTANCE		I2C3
#define I2C_REQUEST_WRITE		0x00
#define I2C_REQUEST_READ		0x01
#define SLAVE_OWN_ADDRESS		0x08
#define TIMEOUT_SETMODE_CMD		5
#define CO2_HEADER1				0x42
#define CO2_HEADER2				0x4D

MACHINE(co2,Driver)
	M_SIGNAL(dataReceived, uint16_t)
	M_TASK(read)
public:
	void init() override;
	void update();
private:
	enum class EventType{Start, FlagADDR, FlagBTF, FlagRXNE};
	STATE_DEF(Idle);
	STATE_DEF(ReceiveN);
	STATE_DEF(Receive3);
	STATE_DEF(Receive2);
private:
	uint8_t data_[CO2_BUFFER_SIZE];
	uint16_t index_ = 0;
private:
	void startRead_();
	void endRead_();
	void calculate_();
MACHINE_END
