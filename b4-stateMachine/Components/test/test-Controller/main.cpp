#include <Controller/controller.h>

int main()
{
	core::Engine::instance().delay(100); // blocking
	COMPONENT_REG(ex, Controller);
	core::Engine::instance().run();
	return 0;
}
