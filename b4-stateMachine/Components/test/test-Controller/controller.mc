Project{name:'test-controller'}

Section{name:'MODE'}
DoubleButton{nameLeft: 'Auto',nameRight: 'Manual',commandLeft:102, commandRight: 103}

Section{name:'PANEL'}
DoubleButton{nameLeft: 'SpeedUp',nameRight: 'SpeedDown',commandLeft:100, commandRight: 101}

Section{name:'Speed'}
Plot{name:'Speed', color:'cyan',channel:0,scale: 0.3, offset: 200}
