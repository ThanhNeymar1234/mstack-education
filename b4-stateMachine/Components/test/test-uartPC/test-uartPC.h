#ifndef TEST_TEST_UARTPC_TEST_UARTPC_H_
#define TEST_TEST_UARTPC_TEST_UARTPC_H_

#include <core/engine.h>
#include <core/task.h>

COMPONENT(ex, testUart)

	M_TASK(send2PC)

public:
	void init() override;

private:

COMPONENT_END

#endif /* TEST_TEST_UARTPC_TEST_UARTPC_H_ */
