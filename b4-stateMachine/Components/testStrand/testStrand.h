#ifndef TESTSTRAND_TESTSTRAND_H_
#define TESTSTRAND_TESTSTRAND_H_

#include <core/engine.h>
#include <core/strand.h>
#include <console/controller.h>
#include <lcd/controller.h>

COMPONENT(ex, STRAND)

	M_STRAND(test, 128)
	M_EVENT(a)

	U_ACTION(100, A)
	U_ACTION(101, B)

	struct Data
	{
		uint32_t a = 5;
		uint32_t b = 10;
	};

	M_EVENT(b, Data)

public:
	void init() override;

private:
	Data a_;

COMPONENT_END

#endif /* TESTSTRAND_TESTSTRAND_H_ */
