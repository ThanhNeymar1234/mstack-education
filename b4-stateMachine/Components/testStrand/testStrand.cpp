#include <console/log.h>
#include <testStrand/testStrand.h>

void ex::STRAND::init()
{
	lcd::Controller::instance().init();
	lcd::Controller::instance().gotoxy(1, 1);
	lcd::Controller::instance().printf("Hello");
}

U_ACTION_HANDLER(ex::STRAND, A)
{
	testStrand.post(&aEvent);
}

U_ACTION_HANDLER(ex::STRAND, B)
{
	testStrand.post<Data>(&bEvent, a_);
}

M_EVENT_HANDLER(ex::STRAND, a)
{
	LOG_PRINTF("Event a duoc goi");
	testStrand.done();
}

M_EVENT_HANDLER(ex::STRAND, b, Data)
{
	LOG_PRINTF("Event b duoc goi : %d %d", event.a, event.b);
	testStrand.done();
}
