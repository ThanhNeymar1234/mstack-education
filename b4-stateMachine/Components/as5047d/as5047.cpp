#include <as5047d/as5047.h>
#include <as5047d/as5047-def.h>
#include <console/log.h>
#include "spi.h"

void ex::AS5047::init()
{
	MX_SPI1_Init();
	LL_SPI_Enable(SPI1);
	LL_SPI_EnableIT_RXNE(SPI1);
	LL_SPI_DisableIT_TXE(SPI1);
}

void ex::AS5047::updateAngle(uint16_t value)
{
	uint16_t a;
	a = 0x4000-(value & 0x3FFF);
	angle_ = a ;
	if(angle_ < 0) angle_ += 0x4000;

	MC_PLOT(0, angle_);
	readAngleStrand.done();
}

void ex::AS5047::read_()
{
	CLEAR_SCS_AS5047;	// pull CS to Low
	SPI1->DR = ADDR_ANGLECOM;
}

M_EVENT_HANDLER(ex::AS5047, read)
{
	read_();
}

extern "C" void SPI1_IRQHandler(void)
{
	uint16_t angle;
  /* USER CODE BEGIN SPI1_IRQn 0 */
	if(LL_SPI_IsActiveFlag_RXNE(SPI1))
	{
		SET_SCS_AS5047;
		angle = SPI1->DR;
		ex::AS5047::instance().updateAngle(angle);

	}
}
