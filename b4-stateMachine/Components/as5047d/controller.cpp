#include <as5047d/controller.h>
#include <as5047d/as5047.h>
#include <console/controller.h>

void ex::As5047Controller::init()
{
	COMPONENT_REG(console, Controller);
	COMPONENT_REG(ex, AS5047);
	readEncoderTask_.start(1);
}

M_TASK_HANDLER(ex::As5047Controller, readEncoder)
{
	ex::AS5047::instance().read();
}

