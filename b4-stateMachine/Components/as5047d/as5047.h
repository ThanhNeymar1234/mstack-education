#ifndef AS5047D_AS5047_H_
#define AS5047D_AS5047_H_

#include <core/engine.h>
#include <core/strand.h>

COMPONENT(ex, AS5047)

	M_STRAND(readAngle, 128)
	M_EVENT(read)

public:
	void init() override;
	void updateAngle(uint16_t value);
	void read()
	{
		readAngleStrand.post(&readEvent);
	}

private:
	void read_();

private:
	uint16_t angle_; // 0 - 16535 <-> 0 - (2^(14)-1)
COMPONENT_END

#endif /* AS5047D_AS5047_H_ */
