#ifndef AS5047D_CONTROLLER_H_
#define AS5047D_CONTROLLER_H_

#include <core/engine.h>
#include <core/task.h>

COMPONENT(ex, As5047Controller)

	M_TASK(readEncoder)

public:
	void init() override;

private:

COMPONENT_END

#endif /* AS5047D_CONTROLLER_H_ */
