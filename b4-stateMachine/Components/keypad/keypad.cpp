#include <keypad/keypad.h>
#include "gpio.h"

void ex::Keypad::init()
{
	HAL_GPIO_WritePin(OUT1_PORT, OUT1_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(OUT2_PORT, OUT2_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(OUT3_PORT, OUT3_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(OUT4_PORT, OUT4_PIN, GPIO_PIN_SET);

	SM_START(StartUp);	// SM : State Machine
	// goi den phuong thuc ENTER cua StartUp
}

M_TASK_HANDLER(ex::Keypad, timeout)
{
	SM_POST(Event::Timeout);
}
