#ifndef KEYPAD_KEYPAD_H_
#define KEYPAD_KEYPAD_H_

#include <core/engine.h>
#include <core/machine.h>
#include <core/task.h>
#include <core/signal.h>
#include <keypad/keypad-def.h>

MACHINE(ex, Keypad)

	M_TASK(timeout)
	M_SIGNAL(pressed, uint8_t);


public:
	void init() override;

private:
	STATE_DEF(StartUp)
	STATE_DEF(WriteOut)
	STATE_DEF(ReadIn)

private:
	enum class Event{Timeout, Start, Read};

private:
	uint8_t i_;
	uint8_t code_;
	uint8_t codeTable_[4][4] = {	1,2,3,10,
									4,5,6,11,
									7,8,9,12,
									14,0,15,13,};
	bool btPressed_ = false;

MACHINE_END

#endif /* KEYPAD_KEYPAD_H_ */
