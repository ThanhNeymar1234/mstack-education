#include <keypad/keypad.h>
#include <console/log.h>

STATE_BODY(ex::Keypad::StartUp)
{
	ENTER_()
	{
		LOG_PRINTF("Start Up");
		timeoutTask_.start(1000,1);
	}
	TRANSITION_(Event::Timeout)
	{
		SM_POST(Event::Start);
	}
	TRANSITION_(Event::Start, WriteOut){}
}

STATE_BODY(ex::Keypad::WriteOut)
{
	ENTER_()
	{
//		LOG_PRINTF("Write Out");
		switch (i_) {
			case 0:
			{
				HAL_GPIO_WritePin(OUT1_PORT, OUT1_PIN, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(OUT2_PORT, OUT2_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT3_PORT, OUT3_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT4_PORT, OUT4_PIN, GPIO_PIN_SET);
				break;
			}
			case 1:
			{
				HAL_GPIO_WritePin(OUT1_PORT, OUT1_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT2_PORT, OUT2_PIN, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(OUT3_PORT, OUT3_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT4_PORT, OUT4_PIN, GPIO_PIN_SET);
				break;
			}
			case 2:
			{
				HAL_GPIO_WritePin(OUT1_PORT, OUT1_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT2_PORT, OUT2_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT3_PORT, OUT3_PIN, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(OUT4_PORT, OUT4_PIN, GPIO_PIN_SET);
				break;
			}
			case 3:
			{
				HAL_GPIO_WritePin(OUT1_PORT, OUT1_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT2_PORT, OUT2_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT3_PORT, OUT3_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(OUT4_PORT, OUT4_PIN, GPIO_PIN_RESET);
				break;
			}
		}
		timeoutTask_.start(10, 1);
	}
	TRANSITION_(Event::Timeout, ReadIn){}
}

STATE_BODY(ex::Keypad::ReadIn)
{
	ENTER_()
	{
//		LOG_PRINTF("Read In");
		SM_POST(Event::Read);
	}
	TRANSITION_(Event::Read)
	{
		if(!HAL_GPIO_ReadPin(IN1_PORT, IN1_PIN))
		{
			code_ = codeTable_[i_][0];
			btPressed_ = true;
			i_ = 0;
			SM_SWITCH(WriteOut);
		}
		else if(!HAL_GPIO_ReadPin(IN2_PORT, IN2_PIN))
		{
			code_ = codeTable_[i_][1];
			btPressed_ = true;
			i_ = 0;
			SM_SWITCH(WriteOut);
		}
		else if(!HAL_GPIO_ReadPin(IN3_PORT, IN3_PIN))
		{
			code_ = codeTable_[i_][2];
			btPressed_ = true;
			i_ = 0;
			SM_SWITCH(WriteOut);
		}
		else if(!HAL_GPIO_ReadPin(IN4_PORT, IN4_PIN))
		{
			code_ = codeTable_[i_][3];
			btPressed_ = true;
			i_ = 0;
			SM_SWITCH(WriteOut);
		}
		else
		{
			if(i_ == 3)
			{
				if(btPressed_)	// vua nha nut ra
				{
					pressedSignal.emit(code_);
					btPressed_ = false;
					LOG_PRINTF("Key : %d", code_);
				}
				i_ = 0;
				SM_SWITCH(WriteOut);
			}
			else
			{
				i_++;
				SM_SWITCH(WriteOut);
			}
		}

	}
}
