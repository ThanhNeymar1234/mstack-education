#include <core/task.h>
#include <core/engine.h>

core::Task::Task(Component* component, Handler handler): component_(component), handler_(handler)
{
    core::Engine::instance().registerTask_(this);
}

void core::Task::start(uint32_t interval, int32_t loop)
{
    core::Engine& engine = core::Engine::instance();
    this->interval_ = interval;
    this->nextTick_ = engine.tickCount_ + interval;
    this->loop_ = loop;
    engine.startTask_(this);
}

void core::Task::stop()
{
	this->loop_ = 0;
    core::Engine::instance().stopTask_(this);
    this->nextTick_ = LAST_TICK - 1;
}

void core::Task::run_()
{
    if (--this->loop_ == 0)
    {
        core::Engine::instance().stopTask_(this);
        this->nextTick_ = LAST_TICK - 1;
    }
    else
    {
        this->nextTick_ += this->interval_;
        if (this->loop_ < 0) this->loop_ = -1;
    }

    (component_->*handler_)();
}

