#ifndef A_A_H_
#define A_A_H_

#include <core/engine.h>
#include <core/signal.h>
#include <core/task.h>

COMPONENT(ex, A)

	M_TASK(aTask)

//	M_SIGNAL(aSignal)
//	M_SIGNAL(aSignal_, uint32_t)

	M_SIGNAL_MANY(aSignalMany)
	M_SIGNAL_MANY(aSignalMany_, uint32_t)

public:
	void init() override;

private:
	uint32_t a_ = 0;

COMPONENT_END

#endif /* A_A_H_ */
