#include <A/A.h>

void ex::A::init()
{
	aTaskTask_.start(1000);
}

M_TASK_HANDLER(ex::A, aTask)
{
	a_++;
//	aSignalSignal.emit();		// Empty Signal
//	aSignal_Signal.emit(a_);	// Fixed Signal
	aSignalManySignal.emit();	// Empty Signal
	aSignalMany_Signal.emit(a_);
}
