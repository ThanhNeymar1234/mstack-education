#include <led-indicator/led.h>
#include <console/controller.h>
#include <console/log.h>
#include "gpio.h"

void ex::Led::init()
{
	core::Engine::instance().delay(500);
	COMPONENT_REG(console, Controller);
	blinkLed1Task_.start(1000);
	blinkLed2Task_.start(2000, 4);	// 4 lan
	blinkLed3Task_.start(3000);	// 3 lan
	plotTask_.start(1);
}

M_TASK_HANDLER(ex::Led, plot)
{
	MC_PLOT(0, state1_);
	MC_PLOT(1, state2_);
	MC_PLOT(2, state3_);
}

M_TASK_HANDLER(ex::Led, blinkLed1)
{
//	HAL_GPIO_TogglePin(GPIOx, GPIO_Pin)
	state1_ = !state1_;
}

M_TASK_HANDLER(ex::Led, blinkLed2)
{
	state2_ = !state2_;
}

M_TASK_HANDLER(ex::Led, blinkLed3)
{
	soLan_++;
	if(soLan_ == 3)
	{
		blinkLed3Task_.stop();
	}
	state3_ = !state3_;
}
