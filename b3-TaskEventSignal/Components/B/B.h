#ifndef B_B_H_
#define B_B_H_

#include <core/engine.h>
#include <core/event.h>

COMPONENT(ex, B)

	M_EVENT(catchA)
	M_EVENT(catchA_, uint32_t)

public:
	void init() override;

private:

COMPONENT_END



#endif /* B_B_H_ */
