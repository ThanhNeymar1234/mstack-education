#include <B/B.h>
#include <A/A.h>
#include <C/C.h>
#include <console/log.h>

void ex::B::init()
{
//	A::instance().aSignalSignal.connect(&catchAEvent);	// soft link
//	A::instance().aSignal_Signal.connect(&catchA_Event);
	A::instance().aSignalManySignal.connect(&catchAEvent);
	A::instance().aSignalMany_Signal.connect(&catchA_Event);
}

M_EVENT_HANDLER(ex::B, catchA)
{
	LOG_PRINTF("B Bat duoc Empty Signal");
}

M_EVENT_HANDLER(ex::B, catchA_, uint32_t)
{
	LOG_PRINTF("B Bat duoc Fixed Signal : %d", event);
}
