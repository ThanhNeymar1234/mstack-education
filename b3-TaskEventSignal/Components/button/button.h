#ifndef BUTTON_BUTTON_H_
#define BUTTON_BUTTON_H_

#include <core/engine.h>
#include <core/event.h>
#include <console/controller.h>
#include <core/task.h>

COMPONENT(ex, Button)

	M_EVENT(buttonPressed)	// Empty Event

	U_ACTION(100, button)
	U_TEXT(101, text)

	M_TASK(plot)

public:
	void init() override;

	struct Data
	{
		uint8_t *data;
		uint8_t length;
	};

	M_EVENT(data, Data)		// Fixed Event

private:
	bool state1_;

COMPONENT_END

#endif /* BUTTON_BUTTON_H_ */
