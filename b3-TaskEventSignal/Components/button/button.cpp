#include <button/button.h>
#include <console/log.h>

void ex::Button::init()
{
	COMPONENT_REG(console, Controller);
	plotTask_.start(1);
}

U_ACTION_HANDLER(ex::Button, button)
{
	buttonPressedEvent.post();
}

U_TEXT_HANDLER(ex::Button, text)
{
	Data fake;
	fake.length = length;
	fake.data = data;

	dataEvent.post(fake);
}

M_EVENT_HANDLER(ex::Button, buttonPressed)
{
	state1_ = !state1_;
}

M_EVENT_HANDLER(ex::Button, data, Data)
{
	LOG_PRINTF("Length: %d", event.length);
	LOG_PRINTF("%s", event.data);
}

M_TASK_HANDLER(ex::Button, plot)
{
	MC_PLOT(3, state1_);
}
