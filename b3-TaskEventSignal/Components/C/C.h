#ifndef C_C_H_
#define C_C_H_

#include <core/engine.h>
#include <core/signal.h>

COMPONENT(ex, C)

	M_EVENT(catchA)
	M_EVENT(catchA_, uint32_t)

public:
	void init() override;

private:
	uint32_t c_;

COMPONENT_END


#endif /* C_C_H_ */
