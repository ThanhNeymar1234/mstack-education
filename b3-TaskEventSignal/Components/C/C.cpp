#include <B/B.h>
#include <A/A.h>
#include <C/C.h>
#include <console/log.h>

void ex::C::init()
{
//	A::instance().aSignalSignal.connect(&catchAEvent);	// soft link
//	A::instance().aSignal_Signal.connect(&catchA_Event);
	A::instance().aSignalManySignal.connect(&catchAEvent);
	A::instance().aSignalMany_Signal.connect(&catchA_Event);
}

M_EVENT_HANDLER(ex::C, catchA)
{
	LOG_PRINTF("C Bat duoc Empty Signal");
	if(c_++==3)
	{
		A::instance().aSignalManySignal.disconnect(&catchAEvent);
	}
}

M_EVENT_HANDLER(ex::C, catchA_, uint32_t)
{
	LOG_PRINTF("C Bat duoc Fixed Signal : %d", event);
	if(c_ == 3)
	{
		A::instance().aSignalMany_Signal.disconnect(&catchA_Event);
	}
}
