#include <blink/blink.h>
#include <console/controller.h>
#include <console/log.h>

void ex::LED::init()
{
	COMPONENT_REG(console, Controller);
//	blink1Task_.start(2000, 3);
	blink2Task_.start(3000);
}

M_TASK_HANDLER(ex::LED, blink1)
{
	LOG_PRINTF("Blink 1");
}

M_TASK_HANDLER(ex::LED, blink2)
{
	LOG_PRINTF("Blink 2");
	if(++count_ == 5)
	{
		blink2Task_.stop();
	}
}

