#ifndef BLINK_BLINK_H_
#define BLINK_BLINK_H_

#include <core/engine.h>
#include <core/task.h>

COMPONENT(ex, LED)

	M_TASK(blink1)
	M_TASK(blink2)

public:
	void init() override;

private:
	uint8_t count_ = 0;

COMPONENT_END

#endif /* BLINK_BLINK_H_ */
