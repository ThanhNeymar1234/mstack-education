Project{name:'Thanh'}

Section{name:'PANEL'}
Button{name: 'Say Hello' ,command:100}
DoubleButton{nameLeft: 'Start',nameRight: 'Stop',commandLeft:101, commandRight: 102}

Section{name:'Plot'}
Plot{name:'Plot 0', color:'cyan',channel:0,scale: 0.3, offset: 600}

Section{name:'String'}
TextField{name:'Name', readonly:false, command: 103}

Section{name:'Integer'}
IntField{name:'Interval', min: 10, max: 1000, readonly:false, command: 104}

