#ifndef TEST_TESTCONSOLE_TESTCONSOLE_H_
#define TEST_TESTCONSOLE_TESTCONSOLE_H_

#include <core/engine.h>
#include <core/task.h>
#include <oscilloscope/dual.h>

COMPONENT(ex, testConsole)

	M_TASK(update)
	O_DUAL(osc , 2, 3)

	//Command: 100-999
	U_ACTION(100, sayHello)
	U_ACTION(101, start)
	U_ACTION(102, stop)

	U_TEXT(103, string)

	U_INTEGER(104, integer)

public:
	void init() override;

private:
	uint32_t fakeValue_ = 10;

	uint16_t sine_[400];
	uint16_t cosine_[400];

COMPONENT_END

#endif /* TEST_TESTCONSOLE_TESTCONSOLE_H_ */
