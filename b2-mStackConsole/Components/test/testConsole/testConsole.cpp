#include <test/testConsole/testConsole.h>
#include <console/controller.h>
#include <console/log.h>
#include <math.h>

#define PI 3.1415

void ex::testConsole::init()
{
	COMPONENT_REG(console, Controller);
	console::Controller::instance().printf("Thanh : %d", fakeValue_);
	LOG_PRINTF("Thanh : %d", fakeValue_*2);

	for(int i = 0; i< 400; i++)
	{
		double v = ((double)i/200) * PI;
		int16_t sine = (int16_t)(sin(v) * 512);
		int16_t cosine = (int16_t)(cos(v) * 512);
		sine_[i] = sine + 512;
		cosine_[i] = cosine + 512;
	}

	updateTask_.start(20);	// 1ms
}

M_TASK_HANDLER(ex::testConsole, update)
{
	static uint16_t angle;
//	console::Controller::instance().plot(0, fakeValue_++);
	MC_PLOT(0,sine_[angle]);
//		oscOSC_.plot(sine_[angle], cosine_[angle]);
	if(++angle >= 400)angle = 0;
}

U_ACTION_HANDLER(ex::testConsole, sayHello)
{
	LOG_PRINTF("Say Hello");
}

U_ACTION_HANDLER(ex::testConsole, start)
{
	LOG_PRINTF("Start");
}

U_ACTION_HANDLER(ex::testConsole, stop)
{
	LOG_PRINTF("Stop");
}

U_TEXT_HANDLER(ex::testConsole, string)
{
	LOG_PRINTF("Length: %d", length);
	LOG_PRINTF("%s", data);
}

U_INTEGER_HANDLER(ex::testConsole, integer)
{
	LOG_PRINTF("%d", value);
}
